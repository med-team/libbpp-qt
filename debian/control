Source: libbpp-qt
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Julien Dutheil <julien.dutheil@univ-montp2.fr>,
           Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: dpkg-dev (>= 1.22.5), debhelper-compat (= 13),
               cmake,
               d-shlibs (>= 0.106~),
               qt5-qmake,
               qtdeclarative5-dev,
               libbpp-phyl-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/libbpp-qt
Vcs-Git: https://salsa.debian.org/med-team/libbpp-qt.git
Homepage: http://biopp.univ-montp2.fr/wiki/index.php/Main_Page
Rules-Requires-Root: no

Package: libbpp-qt-dev
Architecture: any
Section: libdevel
Depends: libbpp-qt2t64 (= ${binary:Version}),
         ${misc:Depends},
         ${devlibs:Depends}
Multi-Arch: same
Description: Bio++ Qt Graphic classes library development files
 Bio++ is a set of C++ libraries for Bioinformatics, including sequence
 analysis, phylogenetics, molecular evolution and population genetics.
 Bio++ is Object Oriented and is designed to be both easy to use and
 computer efficient. Bio++ intends to help programmers to write computer
 expensive programs, by providing them a set of re-usable tools.
 .
 Contains development files of the Bio++ graphical classes developed
 with Qt.

Package: libbpp-qt2t64
Provides: ${t64:Provides}
Replaces: libbpp-qt2
Conflicts: libbpp-qt2 (<< ${source:Version})
Architecture: any
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Multi-Arch: same
Description: Bio++ Qt Graphic classes library
 Bio++ is a set of C++ libraries for Bioinformatics, including sequence
 analysis, phylogenetics, molecular evolution and population genetics.
 Bio++ is Object Oriented and is designed to be both easy to use and
 computer efficient. Bio++ intends to help programmers to write computer
 expensive programs, by providing them a set of re-usable tools.
 .
 Contains the Bio++ graphical classes developed with Qt.
